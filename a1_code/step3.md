# Créer une demande de modification

Il est temps de faire une demande de modification ("merge request") pour que le responsable technique du projet valide notre modification et la fusionne sur la branche stable du dépot.

Pour ce faire, rendez-vous sur l'interface gitlab de votre projet, section `merge_requests`

## A vous de jouer

* 😏 Votre binome prend le role du responsable technique
* Créer une Merge Request et préciser comme "reviewer" votre binome
* Ce dernier va pouvoir relire votre code et valider (ou non) vos modifications pour les fusionner