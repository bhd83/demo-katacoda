# Notre première fonctionnalitée

Le client a appris la création du gestionnaire de code source centralisé. Il est content de vos travaux.

Son responsable technique lui a dit qu'on trouvait souvent dans ce type de projet un fichier "README.md" pour documenter le projet dans le gestionnaire de code.

Allons-y !

## Prérequis

🛑 Pour la suite des TP, ces modifications sont nécessaires avant d'aller plus loin

🖱 Cliquer sur le lien pour ouvrir : `fichier.txt`{{open}}

et ajouter une ligne

👍 Et voilà, on peut continuer !

## Travailler sur une branche

Pour implémenter cette fonctionnalité, nous allons tout d'abord créer une branche de travail sur notre dépot local 

```bash
git checkout -b feature/readme
```{{execute}}

Puis créer le fichier pour documenter le projet à la racine du projet

`touch README.md`{{execute}}

Et le modifier 


🖱 Cliquer `README.md`{{open}} pour ouvrir le fichier README.

Ajouter le contenu qui vous semble important. La sauvegarde est automatique


## Publions les modifications

Signalons à git la présence du nouveau fichier
```bash
git add *
```{{execute}}

Ajouter les modifications dans l'arbre des modifications

```bash
git commit -m "Ajout du readme"
```{{execute}}

Publions les modifications ainsi que notre nouvelle branche sur le gestionnaire de code centralisé

```bash
git push -u origin feature/readme
```{{execute}}

Et voilà, une nouvelle branche est publiée sur le gestionnaire centralisé sur une branche. 

Par contre, le code n'est toujours pas disponible sur la branche principale et donc jamais livrée en production.

