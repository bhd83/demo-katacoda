
## Initialisons un dépot git local

Le code source nous ai fourni sur une clé USB et déposé sur notre poste de travail

Allons dans le répertoire qui contient le code 

```bash
cd demo
```{{execute}}

On va maintenant initialiser un dépot git local dans ce répertoire.

```bash
git init
```{{execute}}

A ce stade, du point de vue de git, le dépot ne contient aucun fichier.


Signalons à git la présence des nouveaux fichiers à suivre en gestion de configuration
```bash
git add *
```{{execute}}

Git a maintenant indexé l'ensemble des fichiers du code source et pourra comparer les modifications plus rapidement.

Publions ces fichiers officiellement dans le dépot local

```bash
git commit -m "dépot du code"
```{{execute}}

😭 Une erreure est survenue ! Essayez de la corriger et faire de nouveau la publication à l'aide de la commande `commit`

## Publions le code source

Utilisons la commande `git remote add origin <url>` pour configurer la synchronisation de notre dépot local avec un dépot distant

```bash
git remote add origin https://gitlab.com/doe_ci/doe-session-03/<votre_projet>
```{{execute}}

Et enfin publions les modifications

```bash
git push -u origin master
```{{execute}}

Et voilà, le code source de l'application est déposé dans le gestionnaire de code centralisé de l'entreprise.
Vous pouvez vous connecter sur l'interface web pour vérifier le contenu du projet et naviguer dans l'interface gitlab
