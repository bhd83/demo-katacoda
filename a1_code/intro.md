# Publication de l'application dans le dépot de code

Il est temps maintenant de professionnaliser la gestion de code source au sein de l'entreprise.
Pour cela, nous allez intégrer le code de l'application dans notre gestionnaire centralisé


## Prérequis

🥳 Avant de commencer, assurez-vous des prérequis

* Création d'un compte sur gitlab.com
* Communiquer votre compte au formateur pour vous donner accès au dépot de la formation
* Créer un projet avec votre nom dans le dépot de formation